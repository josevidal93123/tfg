define("cde/components/AjaxRequest/AjaxRequestComponent",["cdf/components/BaseComponent","cdf/Logger","cdf/lib/jquery","cdf/dashboard/Utils"],function(e,r,t,a){
return e.extend({visible:!1,update:function(){this.executeRequest(this)},parseXML:function(e){
if(!e)return null;var t;try{return(new DOMParser).parseFromString(e,"text/xml")}catch(a){
try{return t=new ActiveXObject("Microsoft.XMLDOM"),t.async="false",t.loadXML(e),t;
}catch(a){}}return r.error("XML is invalid or no XML parser found"),null},executeRequest:function(e){
var n=this,o=e.url,i=e.ajaxRequestType,s=e.parameters,d=e.asyncCall;if(void 0==o)return void r.error("Fatal - No url passed");
if(void 0==i&&(i="json"),void 0!=s){for(var u=!1,l=0;l<s.length;l++){var c;"time"==s[l][0]?(c=(new Date).getTime(),
u=!0):c=n.dashboard.getParameterValue(s[l][0]),s[l][1]=c}u?s.push(["time",(new Date).getTime()]):0,
s=a.propertiesArrayToObject(s)}else s={};void 0==d&&(d=!0),t.ajax({url:o,type:"GET",
dataType:i,async:d,data:s,complete:function(a,o){var i=a.responseText,s=void 0;if(void 0==i)return void r.error("Found error: Empty Data");
if("xml"==this.dataType||"html"==this.dataType){if(i=n.parseXML(i),null==i)return;
var d=i.getElementsByTagName("return");if(!(d.length>0&&d[0].firstChild))return;i=d[0].firstChild.nodeValue,
i=t.parseJSON(i)}else if("json"==this.dataType)i=t.parseJSON(i);else if("script"!=this.dataType&&"text"!=this.dataType)return void r.error("Found error: Unknown returned format");
"function"==typeof e.postFetch&&(s=e.postFetch(i)),void 0!=s&&(i=s),void 0!=e.resultvar&&n.dashboard.fireChange(e.resultvar,i);
},error:function(e,t,a){r.error("Found error: "+e+" - "+t+", Error: "+a)}})}})});