var requireConfig=requireCfg.config;requireConfig.amd||(requireConfig.amd={}),requireConfig.amd.shim||(requireConfig.amd.shim={}),
requireConfig.amd.shim["cde/components/googleAnalytics/lib/jquery.ga"]={exports:"jQuery",
deps:{"cdf/lib/jquery":"jQuery"}},requirejs.config(requireCfg),define("cde/components/googleAnalytics/GoogleAnalyticsComponent",["cdf/components/BaseComponent","cdf/lib/jquery","amd!./lib/jquery.ga"],function(e,i){
return e.extend({update:function(){i.ga.load(this.gaTrackingId)}})});