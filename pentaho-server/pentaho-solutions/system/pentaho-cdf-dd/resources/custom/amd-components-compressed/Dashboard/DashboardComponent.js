define("cde/components/Dashboard/DashboardComponent.ext",[],function(){return{getDashboardUrl:function(e){
return e.indexOf("getDashboard?path=")>0?e:"dash!"+encodeURIComponent(e).replace(/[!'()*]/g,function(e){
return"%"+e.charCodeAt(0).toString(16)})},getDashboardParametersEndpoint:function(){
return CONTEXT_PATH+"plugin/pentaho-cdf-dd/api/renderer/getDashboardParameters?path=";
}}}),define("cde/components/Dashboard/DashboardComponent",["cdf/components/UnmanagedComponent","./DashboardComponent.ext","cdf/lib/jquery"],function(e,a,r){
return e.extend({_pause:!1,update:function(){if(!this.preExec())return!1;var e=this;
require([a.getDashboardUrl(this.dashboardPath)],function(a){e.requiredDashboard=new a(e.htmlObject),
e.mapDataSources(),e.unregisterEvents(),e.requiredDashboard.setupDOM(),e.requiredDashboard._processComponents(),
e.mapParameters(function(){e.requiredDashboard.on("cdf:postInit",function(a){e.postExec();
}),e.requiredDashboard.init()})})},mapDataSources:function(){for(var e=0;e<this.dataSourceMapping.length;e++)this.requiredDashboard.setDataSource(this.dataSourceMapping[e][1],this.dashboard.getDataSource(this.dataSourceMapping[e][0]),!0);
},unregisterEvents:function(){if(this.registeredEvents)for(var e in this.registeredEvents)for(var a=0;a<this.registeredEvents[e].length;a++)this.dashboard.off(e,this.registeredEvents[e][a]);
},mapParameters:function(e){var t=this,n=this.requiredDashboard;this.registeredEvents={},
this.publicParameters=[];var s=a.getDashboardParametersEndpoint()+this.dashboardPath;
r.ajax({url:s,type:"GET",async:!0,success:function(a){t.publicParameters=a.parameters||[],
t.loopThroughMapping(function(e,a){if(t.isParameterPublic(a)){var r=e+":fireChange",s=function(e){
t._pause||n.getParameterValue(a)===e.value||(t.loopThroughMapping(function(e,a){n.setParameter(a,t.dashboard.getParameterValue(e));
}),n.fireChange(a,e.value))};t.dashboard.on(r,s),n.on(a+":fireChange",function(a){
t._pause||0!=t.oneWayMap||t.dashboard.getParameterValue(e)===a.value||(t.loopThroughMapping(function(e,a){
t.dashboard.setParameter(e,n.getParameterValue(a))}),t.dashboard.fireChange(e,a.value));
}),t.registeredEvents[r]||(t.registeredEvents[r]=[]),t.registeredEvents[r].push(s),
t.requiredDashboard.setParameter(a,t.dashboard.getParameterValue(e))}}),e()},error:function(e){
t.failExec(e)},xhrFields:{withCredentials:!0}})},pausePropagation:function(){this._pause=!0;
},resumePropagation:function(){this._pause=!1},loopThroughMapping:function(e){for(var a=0;a<this.parameterMapping.length;a++)e(this.parameterMapping[a][0],this.parameterMapping[a][1]);
},isParameterPublic:function(e){for(var a=0;a<this.publicParameters.length;a++)if(e===this.publicParameters[a])return!0;
return!1}})});