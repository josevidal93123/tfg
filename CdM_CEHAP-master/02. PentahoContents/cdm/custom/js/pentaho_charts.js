var selectorPrefix = 'Seleccione ';

function createSelectComboFilters() {
    $(".pq-select").each(function() {
        var label = $(this).attr("name");
        var id = $(this).attr("id") + '_pq_select';
        var maxDisplay = $(this).attr("maxDisplay");
        var selectCls = $(this).attr("selectCls");
		var type = $(this).attr("type");
		var multiple = $(this).attr("multiple");
		var isMultiple = true;
		if (type == FILTER_TYPE.CUSTOM) {
			//do nothing
		} else if (type != undefined && type == FILTER_TYPE.SINGLE){
			$(this).removeAttr("multiple");
			isMultiple = false;
		} else if (multiple == undefined && isMultiple){
			$(this).attr("multiple", "multiple");
		}
        $(this).pqSelect({
            multiplePlaceholder: isMultiple ? selectorPrefix + label : null,
			singlePlaceholder: !isMultiple ? selectorPrefix + label : null,
            checkbox: isMultiple,
			radio: !isMultiple,
            selectallText: "Seleccionar todos",
            closeOnWindowResize: true,
            closeOnWindowScroll: true,
            selectCls: selectCls != undefined || selectCls != null ? selectCls : '',
            maxDisplay: maxDisplay != undefined || maxDisplay != null ? maxDisplay : 0,
            displayText: "<span style='color: #e06325;'>{0}</span> de {1} " + label
        });

        $('label[for="' + $(this).attr("id") + '"]').text(label + ":");
    });
}

function clearFilters(e) {
    var checkbox = ($("#" + e).parent().parent().find(".pq-select-all").find("input[type=checkbox]"));
    if (checkbox.is(":checked")) {
        checkbox.click();
    } else {
        checkbox.click().click();
    }
    var radio = ($("#" + e).parent().parent().parent().parent().find(".pq-select-option-label > .pq-state-select > .ui-state-enable > .pq-state-hover").find("input[type=radio]"));
    if (radio.is(":checked")) {
        radio.click();
    } else {
        radio.click().click();
    }
}

function pqSelectPopulation(filter) {
    var selector = filter.selectorId;
    var columnName = filter.queryColumnName;
    $('#' + selector).empty();
	if (filter.filterType == FILTER_TYPE.SINGLE && $('#' + selector).attr('required') == undefined) {
		$('#' + selector).append('<option value="@@@___@@@"></option>');
	}
    $(filter.getOptions()).each(function(i, data) {
        filter.jsonResponse[i].text = data;
        var selected = "";
        selected = filter.isOptionSelected(data) ? "selected" : "";
        $('#' + selector).append('<option value="' + data.split('"').join('&quot') + '" label="' + columnName + '" ' + selected + ' >' + data + '</option>');
    });

    $('#' + selector).pqSelect("refreshData");
}

function positionerTooltip(boxWidth, boxHeight, point) {
    // Set up the variables
    var chart = this.chart,
        plotLeft = chart.plotLeft,
        plotTop = chart.plotTop,
        plotWidth = chart.plotWidth,
        plotHeight = chart.plotHeight,
        distance = 12, //pick(this.options.distance, 12), // You can use a number directly here, as you may not be able to use pick, as its an internal highchart function
        pointX = point.plotX,
        pointY = point.plotY,
        x = pointX + plotLeft + (chart.inverted ? distance : -boxWidth - distance),
        y = pointY - boxHeight + plotTop + 15, // 15 means the point is 15 pixels up from the bottom of the tooltip
        alignedRight;

    // It is too far to the left, adjust it
    if (x < 7) {
        x = plotLeft + pointX + distance;
    }

    // Test to see if the tooltip is too far to the right,
    // if it is, move it back to be inside and then up to not cover the point.
    if ((x + boxWidth) > (plotLeft + plotWidth)) {
        x -= (x + boxWidth) - (plotLeft + plotWidth);
        y = pointY - boxHeight + plotTop - distance;
        alignedRight = true;
    }

    // If it is now above the plot area, align it to the top of the plot area
    if (y < plotTop + 5) {
        y = plotTop + 5;

        // If the tooltip is still covering the point, move it below instead
        if (alignedRight && pointY >= y && pointY <= (y + boxHeight)) {
            y = pointY + plotTop + distance; // below
        }
    }

    // Now if the tooltip is below the chart, move it up. It's better to cover the
    // point than to disappear outside the chart. #834.
    if (y + boxHeight > plotTop + plotHeight) {
        if (plotTop >= plotTop + plotHeight - boxHeight - distance) {
            y = plotTop;
        } else {
            y = plotTop + plotHeight - boxHeight - distance;
        }
    }


    return { x: x, y: y };
}


function appFilter(evt) {
    
    if (evt != null) {
        recalculateFilters(evt.delegateTarget.id);
		renderCharts();
    }
}

/* Controla el cambio de color del icono de filtros */
function appFilterSelect(evt) {
    appFilter(evt);
}

function resizeCharts() {
    setTimeout(function() {
        $(".highcharts-container:visible").each(function(i, elem) {
            var chart = $($(elem).parent()[0]).highcharts();
            var container = chart.container;
            chart.reflow();
        });
    }, 10);
}