var URLBase = window.location.protocol + "//" + window.location.host;

var monthsName = { 'enero': '01', 'febrero': '02', 'marzo': '03', 'abril': '04', 'mayo': '05', 'junio': '06', 'julio': '07', 'agosto': '08', 'septiembre': '09', 'octubre': '10', 'noviembre': '11', 'diciembre': '12' };
var months = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
var shortMonths = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];


var sqlObjectResponse = {};

var FILTERS_OBJECT_ARRAY = [];

var SQL_OBJECT_ARRAY = [];

var SQL_OBJECT_FILTER_ARRAY = new Object();

var arr=[];

var FILTER_TYPE = {
    SINGLE: "single",
	RADIO: "single",
    MULTIPLE: "multiple",
	CHECKBOX: "multiple",
	CUSTOM: "custom"
};

var loadHTML = true;

$(document).ready(function() {
	$(document).ajaxStop(function (){
		if (loadHTML && (document.URL).indexOf('generatedContent') > -1){
			$('body').load('/cdm_chie/cdm/content/DashboardANS.html');
			loadHTML = false;
		}
	});
	loadContext();
});

var delay = (function() {
    var timer = 0;
    return function(callback, ms){
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function loadContext() {
	var a = getPentahoContext();
	var pentahoContext = a.responseText;
	var lines = (pentahoContext.toString()).split("\n");
	var pentahoScript = "";
	$(lines).each(function(i,d){
		if (d.indexOf("document.write") < 0) {
			pentahoScript += "\n" + d;
		}
	});
	
	var x = document.createElement("SCRIPT");
    var t = document.createTextNode(pentahoScript);
    x.appendChild(t);
	document.body.appendChild(x);
}

function getPentahoContext() {
	return $.ajax({
		url: URLBase + "/cdm_chie/mantle/home/webcontext.js",
		type: "GET",
		async: false,
		dataType: "json"
	});
}

$(function(){
    $("[data-load]").each(function(){
        $(this).load($(this).data("load"), function(){
        });
    });
	$( document ).ajaxStop(function() {
		$(".main").fadeIn();
	});
})

function DataPentahoRequest(path, dataAccessId){
    //Atributos básicos de la petición.
	this.path = path;
    this.dataAccessId = dataAccessId;
	this.outputIndexId = 1;
	this.pageSize = 0;
	this.pageStart = 0;
	this.sortBy;
	this.paramsearchBox;
	
	//Obtiene un parámetro en función de la clave pasada por parámetro.
	this.getParameterValue = function(param) {
        return this[param];
    }

	//Añade un parámetro a una clave pasada por parámetro, si no existe, la crea.
    this.addParameter = function(param, value) {
		if (this instanceof DataPentahoRequest){
			if(this[param]==undefined) {
				this[param] = value;
			} else {
				this[param] += ";" + value;
			}
		}			
	}

	//Elimina un parámetro en función de la clave pasada por parámetro.
    this.removeParameter = function(param) {
        delete this[param];
    }
}

function Filter(filterName, jsonResponse, queryColumnName, selectorId, queryParameter, onPostCall, onChangeEvent, filterType) {
    this.filterName = filterName;
    this.jsonResponse = jsonResponse;
    this.queryColumnName = queryColumnName;
	if (filterType == undefined) {
		this.filterType = FILTER_TYPE.CUSTOM;
	} else if (filterType == undefined){
		this.filterType = FILTER_TYPE.MULTIPLE;
	} else {
		this.filterType = filterType;
	}
    this.selectorId = selectorId;
    this.queryParameter = queryParameter;
    this.checkedOptions = [];

    //Return all options values
    this.getOptions = function() {
        var options = [];
        $(this.jsonResponse).each(function() {
            options.push(this[queryColumnName]);
        });
        return options;
    }

    //Returns all checked options values
    this.getCheckedOptions = function() {
        return this.checkedOptions;
    }

    //Adds an option to the checked array
    this.addCheckedOption = function(value) {
        if (this.checkedOptions.indexOf(value) === -1) {
            this.checkedOptions.push(value);
        }
    }

    //Removes an option to the checked array
    this.removeCheckedOption = function(param) {
        var index = this.checkedOptions.indexOf(param);

        if (index > -1) {
            this.checkedOptions.splice(index, 1);
        }
    }

    //Returns the number of options
    this.getOptionSize = function() {
        return this.getOptions().length;
    }

    //Returns the number of checked options
    this.getCheckedOptionSize = function() {
        var res;
        if (this.getCheckedOptions() != undefined) {
            res = this.getCheckedOptions().length;
        } else {
            res = 0;
        }
        return res;
    }

    //Returns if an option is selected
    this.isOptionSelected = function(param) {
        var res;
        if (this.getCheckedOptions() != undefined) {
            res = this.getCheckedOptions().indexOf(param) > -1;
        } else {
            res = false;
        }
        return res;
    }

    //Returns if has an option selected
    this.hasCheckedOptions = function() {
        if (this.getCheckedOptionSize() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //Returns if all possible options are selected
    this.isAllSelected = function() {
        if (this.getOptionSize() > 0 && this.getOptionSize() == this.getCheckedOptionSize()) {
            return true;
        } else {
            return false;
        }
    }

    //Return if has options for selection
    this.isEmpty = function() {
        if (this.getOptionSize() == 0) {
            return true;
        } else {
            return false;
        }
    }

    //Return a ## separated string of checked options
    this.getCheckedOptionsString = function() {
        return this.getCheckedOptions().join("##");
    }

    this.postCall = function() {
        onPostCall(this);
    }

    var f = this;

    $("#" + this.selectorId).on("change", function(evt) {
        f.checkedOptions = f.filterType != 'single'? $(this).val(): [$(this).val()];

        delay(function() {
            onChangeEvent(evt);
        }, 500);
    });

    this.refreshOptions = function() {
        var sqlAux = SQL_OBJECT_FILTER_ARRAY[f.queryParameter];
        for (var i = 0; i < FILTERS_OBJECT_ARRAY.length; i++) {
            var filterAux = FILTERS_OBJECT_ARRAY[i];
            if (filterAux.filterName != f.filterName) {
                sqlAux.removeParameter(FILTERS_OBJECT_ARRAY[i].queryParameter);
                if (filterAux.getCheckedOptionSize() == 0) {
                    sqlAux.addParameter(filterAux.queryParameter, "@@@___@@@");
                } else {
                    $.each(filterAux.getCheckedOptions(), function(i, d) {
                        sqlAux.addParameter(filterAux.queryParameter, d);
                    });
                }
            }
            sqlAux.removeParameter(f.queryParameter);
            if (f.getCheckedOptionSize() == 0) {
                sqlAux.addParameter(f.queryParameter, "@@@___@@@");
            } else {
                $.each(f.getCheckedOptions(), function() {
                    sqlAux.addParameter(f.queryParameter, this);
                });
            }
        }

        $.ajax({
			url: URLBase + '/cdm_chie/plugin/cda/api/doQuery',
            type: "POST",
            async: false,
            data: sqlAux,
			queryAlias: sqlAux.dataAccessId,
            success: updateFiltersOptions,
            filter: f,
            dataType: "json"
        });

    }
}

// Ejecuta un comando ajax para consultar la Query
function getJSONFilter(data, asyncMethod, filterName, selector, columnName, parameterName, onPostCall, onChange, filterType) {
	var query = $.ajax({
        url: URLBase + '/cdm_chie/plugin/cda/api/doQuery',
        type: "POST",
        async: asyncMethod,
        data: data,
		queryAlias: data.dataAccessId,
        success: createNewFilter,
        dataType: "json"
    });

    function createNewFilter(xmlResponse) {
		tratarRespuesta(xmlResponse, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
        var filter = new Filter(filterName, jsonResponse, columnName, selector, parameterName, onPostCall, onChange, filterType);
        if (window.opener != null && window.opener.Parameters != null && getURLParameter("Op") == 1) {
            filter.checkedOptions = window.opener.Parameters[filterName];
        } else if (sessionStorage.getItem(filter.filterName) != null) {
            var values = sessionStorage.getItem(filter.filterName).split("##");
            for (var i = 0; i < values.length; i++) {
                filter.addCheckedOption(values[i]);
            }
        } else if (localStorage.getItem(filter.filterName) != null) {
            var values = localStorage.getItem(filter.filterName).split("##");
            for (var i = 0; i < values.length; i++) {
                filter.addCheckedOption(values[i]);
            }
        }

        filter.postCall();
        addFilterObject(filter);
    }
    return query;
}

function updateFiltersOptions(xmlResponse) {
	tratarRespuesta(xmlResponse, this.queryAlias);
	var jsonResponse = getResponseFromQuery(this.queryAlias);
    this.filter.jsonResponse = jsonResponse;
    this.filter.postCall();
}

function addFilterObject(filter) {
    var enc = false;
    var i, j;
    for (i = 0; i < FILTERS_OBJECT_ARRAY.length && !enc; i++) {
        if (FILTERS_OBJECT_ARRAY[i].filterName == filter.filterName) {
            enc = true;
			j = i;
        }
    }

    if (enc) {
        FILTERS_OBJECT_ARRAY.splice(j, 1);
    }

    FILTERS_OBJECT_ARRAY.push(filter);
}

function getFilterByName(filterName) {
    var i;
    var result = -1;
    for (i = 0; i < FILTERS_OBJECT_ARRAY.length; i++) {
        if (FILTERS_OBJECT_ARRAY[i].filterName == filterName) {
            result = FILTERS_OBJECT_ARRAY[i];
            break;
        }
    }
    return result;
}

function createParametersSQLObjects() {
    clearParametersSQLObjects();
    for (var i in SQL_OBJECT_ARRAY) {
        var sqlAux = SQL_OBJECT_ARRAY[i];
        for (var j in FILTERS_OBJECT_ARRAY) {
            var filterAux = FILTERS_OBJECT_ARRAY[j];
            if (filterAux.getCheckedOptionSize() == 0) {
                sqlAux.addParameter(filterAux.queryParameter, "@@@___@@@");
            } else {
                $.each(filterAux.getCheckedOptions(), function() {
                    sqlAux.addParameter(filterAux.queryParameter, this);
                });
            }
        }

        if (FILTERS_OBJECT_ARRAY.length == 0) {
            for (var parameter in SQL_OBJECT_FILTER_ARRAY) {
                sqlAux.addParameter(parameter, "@@@___@@@");
            }
        }
    }
}

function recalculateFilters(selector) {
    for (var i = 0; i < FILTERS_OBJECT_ARRAY.length; i++) {
        if (selector != FILTERS_OBJECT_ARRAY[i].selectorId) {
            FILTERS_OBJECT_ARRAY[i].refreshOptions();
        }
    }
}

function clearParametersSQLObjects() {
    for (var i in SQL_OBJECT_ARRAY) {
        for (var parameter in SQL_OBJECT_FILTER_ARRAY) {
            SQL_OBJECT_ARRAY[i].removeParameter(parameter);
        }
    }
}

function getPentahoRequest(successFunction, data, async) {
    return $.ajax({
        url: URLBase + '/cdm_chie/plugin/cda/api/doQuery',
        type: "POST",
        data: data,
		queryAlias: data.dataAccessId,
        async: async,
        success: successFunction,
		dataType: "json"
    });
}
///////////////////////

function buildFilterDateRange(selector, data, viewMode, renderFunction, format, async,choice) {
    if (selector) { selector = $("#"+selector) }
    if (async == null) { async = true }

	function fillCalendar(items) {
		
		tratarRespuesta(items, this.queryAlias);
		
		var parsedResponse = getResponseFromQuery(this.queryAlias)[0];
		var start = parsedResponse.DAYLASTDATE+'-'+parsedResponse.MONTHLASTDATELESS+'-'+parsedResponse.YEARLASTDATE;
		var end = parsedResponse.DAYLASTDATE+'-'+parsedResponse.MONTHLASTDATE+'-'+parsedResponse.YEARLASTDATE;
		var startDate = parsedResponse.DAYFIRSTDATE+'-'+parsedResponse.MONTHFIRSTDATE+'-'+parsedResponse.YEARFIRSTDATE;
		var endDate = parsedResponse.DAYLASTDATE+'-'+parsedResponse.MONTHLASTDATE+'-'+parsedResponse.YEARLASTDATE;
		
		selector.find('input').daterangepicker({
		opens:'left',
		drops:'down',
		locale: {
        format: "DD-MM-YYYY",
        separator: " - ",
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        customRangeLabel: "Custom",
        weekLabel: "W",
        daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: months,
        firstDay: 1
    },
			minDate: startDate,
			maxDate:endDate,
			startDate: start,
			endDate: end
		}, function(start,end){
			//selector.find("input").html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
			  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
			
		});

		selector.on('apply.daterangepicker', function(ev,picker) {
			try {
				//var range = selector.data('daterangepicker');
				setReportingPeriod(selector,picker,picker.startDate,picker.endDate);
				renderFunction(selector);
			} catch (err) {
				console.log("Dashboard, not render.")
			}
		});	
		renderFunction(selector);
	}
	
    return $.ajax({
        url: URLBase + '/cdm_chie/plugin/cda/api/doQuery',
        type: "POST",
        async: async,
        data: data,
		queryAlias: data.dataAccessId,
        calendarSelector: selector,
        success: fillCalendar
    });
	
	
}

function setReportingPeriod(selector,range,startDate,endDate) {
    try {
		
		range.setStartDate(startDate);
		range.setEndDate(endDate);
		selector.find("input").html(startDate.format('DD-MM-yyyy') + ' - ' + endDate.format('DD-MM-yyyy'));
		
    } catch (err) {
		console.log(err);
        console.log("error setReportingPeriod");
    }

};

function getReportingPeriod(selector) {
    try {
		
        var start;
		var end;
		
        if (selector[0].id=='ReportingPeriodRange') {
            start = selector.find('input').val().split(' - ')[0];
			end = selector.find('input').val().split(' - ')[1];
			
        } else {
            date = selector.find('input').val();
			
        }
    } catch (err) {
        console.log("error setReportingPeriod");
    }
    return {start:start, end:end};
};






///////
function buildFilterDateANS(selector, data, viewMode, renderFunction, format, async,choice) {
    if (selector) { selector = $("#"+selector) }
    if (format == null) { format = 'MMMM, YYYY' }
    if (async == null) { async = true }

	function fillCalendarANS(items) {
		
		tratarRespuesta(items, this.queryAlias);
		
		var parsedResponse = getResponseFromQuery(this.queryAlias)[0];
		
		this.calendarSelector.datetimepicker({
			viewMode: viewMode,
			minDate: viewMode != 'days' ? moment((format == 'YYYY' ? '01' : parsedResponse.MONTHFIRSTDATE) + "/01/" + parsedResponse.YEARFIRSTDATE) : moment(parsedResponse.MONTHFIRSTDATE + "/" + parsedResponse.DAYFIRSTDATE + "/" + parsedResponse.YEARFIRSTDATE),
			maxDate: viewMode != 'days' ? moment(parsedResponse.MONTHLASTDATE + "/01/" + parsedResponse.YEARLASTDATE) : moment(parsedResponse.MONTHLASTDATE + "/" + parsedResponse.DAYLASTDATE + "/" + parsedResponse.YEARLASTDATE).endOf('day'),
			format: format,
			locale: 'es'

		});

		// So se pasa por sesión, se tiene en cuenta en último lugar
		if (sessionStorage.getItem("Y") != null && sessionStorage.getItem("M") != null) {
			this.calendarSelector.data("DateTimePicker").date(moment(sessionStorage.getItem("M") + "/01/" + sessionStorage.getItem("Y")));
		}

		setReportingPeriodANS(this.calendarSelector);
		this.calendarSelector.on('dp.change', function(ev) {
			try {
				setReportingPeriodANS($(this));
				renderFunction();
			} catch (err) {
				console.log("Dashboard, not render.")
			}
		});
		this.calendarSelector.datetimepicker().on('dp.show dp.update', function(e) {
			//setReportingPeriod($(this));
			$(this).find(".datepicker-years .picker-switch").removeAttr('title')
				//.css('cursor', 'default')
				//.css('background', 'inherit')
				.on('click', function(e) {
					e.stopPropagation();
				});
		});		
		renderFunction();
	}
	
    return $.ajax({
        url: URLBase + '/cdm_chie/plugin/cda/api/doQuery',
        type: "POST",
        async: async,
        data: data,
		queryAlias: data.dataAccessId,
        calendarSelector: selector,
        success: fillCalendarANS
    });
	
	
}

function setReportingPeriodANS(selector) {
    try {
        var month;
        var year;
        selector.find('input').attr('value', selector.find('input').val());
        if (selector.find('input').val().split(", ").length > 1) {
            year = selector.find('input').val().split(", ")[1];
            month = monthsName[selector.find('input').val().split(", ")[0]];
            var dateText = selector.find(".dateText");
            if (dateText.length > 0) {
                dateText.html(months[parseInt(month) - 1] + "," + year);
            }
        }
        else if(selector.data("DateTimePicker").maxDate())
        {
            selector.data("DateTimePicker").date($("#ReportingPeriodCalendar").data("DateTimePicker").maxDate());
        }
        else {
            month = 12;
            year = selector.find('input').val();
            var dateText = selector.find(".dateText");
            if (dateText.length > 0) {
                dateText.html(year);
            }
        }
    } catch (err) {
        console.log("error setReportingPeriod");
    }

};

function getReportingPeriodANS(selector) {
    try {
        var month;
        var year;
        if (selector.find('input').val().split(", ").length > 1) {
            year = selector.find('input').val().split(", ")[1];
            month = monthsName[selector.find('input').val().split(", ")[0]];
        } else {
            month = 12;
            year = selector.find('input').val();
        }
    } catch (err) {
        console.log("error setReportingPeriod");
    }
    return { month: month, year: year };
};
///////






function tratarRespuesta(response, functionName) {
	var responseArray = [];
	var responseMetadata = response["metadata"];
	var responseResultSet = response["resultset"];
	$(responseResultSet).each(function(i,d){
		var responseObject = {};
		$(d).each(function(j,e) {
			responseObject[responseMetadata[j]["colName"]] = e;
		});
		responseArray.push(responseObject);
	});
	sqlObjectResponse[functionName] = responseArray;
}

function getResponseFromQuery(key){
	if(sqlObjectResponse.hasOwnProperty(key)) {
		return sqlObjectResponse[key];
	}
}

function doneLoadingCallback(){};
/////////////////////////////////////////////
(function() {
    
    // select the target node
    var target;
    var timeoutHandle;

    // create an observer instance
    var observer = new MutationObserver(function(mutations) {
        window.clearTimeout(timeoutHandle);
        timeoutHandle = window.setTimeout(resolvePageLoading, window.loadingIntervalCheck);
    });

    function resolvePageLoading()
    {
        if(document.readyState != 'complete')
        {
            window.clearTimeout(timeoutHandle);
            timeoutHandle = window.setTimeout(resolvePageLoading);
        }
        else
        {
            window.clearTimeout(timeoutHandle);
            observer.disconnect();
            window.doneLoading = true;
            console.log("Page loaded!!");
			$("#userLogged").text(window.SESSION_NAME);
			$.ajax({
				type: "GET",
				url: URLBase + "/cdm_chie/api/repos/%3Ahome%3Aadmin%3AUploadFiles.wcdf/generatedContent",
				success: function(data) { $("#uploadFileOption").show(); },
				error: function(data) { $("#uploadFileOption").hide(); }
			});
            if(doneLoadingCallback)
                doneLoadingCallback();    
        }
    }

    $( document ).ready(function(){
        // configuration of the observer:
        var config = { attributes: true, childList: true, characterData: true, subtree: true }
    
        target = document.querySelector('body');

        // pass in the target node, as well as the observer options
        observer.observe(target, config);
    });
    
})();

function autoFormatNumber(num, n) {
    if (num != null) {
        if (num == Math.floor(num))
            return numeral(num).format("0a");
        var a = Math.abs(num); // Absolute value of number
        var la = Math.log(a) / Math.LN10; // Log10 of absolute value
        var ld = Math.floor(la) % 3 + 1; // Digits to the left of .
        //var rd = decimalPlaces(num); // Digital to the right of .

        var fmt = "0.";
        for (var i = 0; i < n; i++) {
            fmt = fmt + "0";
        }
        fmt = fmt + "a";

        return numeral(num).format(fmt);
    }
    return null
};

function parseDate(date){
	
	
	var d = new Date(date)
	
  let month = String(d.getMonth() + 1);
  let day = String(d.getDate());
  const year = String(d.getFullYear());

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return day+'-'+month+'-'+year;
  //  return '${day}-${month}-${year}';


	
}

function toEuros(n){
	if (n ==null){
		n = 0;
	} 
  return n.toLocaleString('es-ES',{minimumFractionDigits: 2, style: 'currency', currency: 'EUR' });
}

function truncate(num, n) {
    if (num != null) {
        if (num == Math.floor(num))
            return numeral(num).format("0,0");
        var a = Math.abs(num); // Absolute value of number
        var la = Math.log(a) / Math.LN10; // Log10 of absolute value
        var ld = Math.floor(la) % 3 + 1; // Digits to the left of .
        //var rd = decimalPlaces(num); // Digital to the right of .

        var fmt = "0,0.";
        for (var i = 0; i < n; i++) {
            fmt = fmt + "0";
        }

        return numeral(num).format(fmt);
    }
    return null
};