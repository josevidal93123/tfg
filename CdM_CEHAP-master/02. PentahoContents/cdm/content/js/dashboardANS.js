var reportingPeriod;
var showCategoryKPIs = true;
var firstLoad = true;
var data;

$(document).ready(function(){
	$("[data-load]").each(function(){
		$(this).load($(this).data("load"), function(){
			//$("#homeTab").addClass("active");
			//removeLoading();
			$(".main").fadeIn();
			$("#userLogged").text(window.SESSION_NAME);
		});
	});
	
	
	var dataRequestCalendar =  new DataPentahoRequest('/public/DashboardANSView.cda', 'dsReportingPeriod');
	buildFilterDateANS('ReportingPeriodCalendar', dataRequestCalendar, 'months', init, null, true);
	$( document ).ajaxStop(function() {
		if (firstLoad) {
			$("#dashboardANSTab").addClass("active");
			firstLoad = false;
		}
		removeLoading();
		resizeCharts();
	});
});


function init(){
	reportingPeriod = getReportingPeriodANS($("#ReportingPeriodCalendar"));
	renderCharts();
}

function renderCharts(){
	var dataRequestDashboard =  new DataPentahoRequest('/public/DashboardANSView.cda', 'dsAnsDashboard');
	dataRequestDashboard.addParameter("paramYear", reportingPeriod.year);
	dataRequestDashboard.addParameter("paramMonth", reportingPeriod.month);
	getPentahoRequest(renderDashboard, dataRequestDashboard, false);
	
	function renderDashboard(response){
		tratarRespuesta(response, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
		data = getEvolutionStatusTrendGroupedBy(jsonResponse, 'CODE', { year: reportingPeriod.year, month: reportingPeriod.month });
		categories = {};
		countFamilyInDataMap = {};
		totalKPIsCount = data.length;
		countFamilyInData(data);
		$.each(data, function(i, dataValue) {
			if (dataValue.category in categories) {
				categories[dataValue.category].push(dataValue);
			} else {
				categories[dataValue.category] = [];
				categories[dataValue.category].push(dataValue);
				var width = calculateColumCategory(countFamilyInDataMap[dataValue.category], data.length);
				categories[dataValue.category]['widthCategory'] = width.widthCategory;
				categories[dataValue.category]['widthCard'] = width.widthCard;
			}
		});
		var isOrange = false;
		var datos = data;
		var tBody = $('.table').find("tbody");
		tBody.empty();
		//Header
		$('body').scroll(function() {
			var sticky = $('thead #header'),
				scroll = $('body').scrollTop();

			if (scroll >= 247) {
				$('#header').show();
				sticky.addClass('fixed');
				var th = $($('.table').find("tbody").find('tr')[1]).children();
				$.each(th, function(indexInArray, valueOfElement) {
					$($('thead #header').find('th')[indexInArray]).width($(valueOfElement).width());
				});
			} else {
				$('#header').hide();
				sticky.removeClass('fixed');
			}

		});

		var idChartNum = 0;
		$.each(categories, function(keyCategory, categoryValue) {
			var tr = $('<tr>').appendTo(tBody);
			var categoryName = categoryValue[0].category == null ? categoryValue[0].group : categoryValue[0].category;
			tr.append("<td colspan='6'' class='category_kpi' data-target='.accordion-" + (categoryName.replace(/\s/g, "-")).safeCSSId() + "' class='accordion-toggle'>" + categoryName + "</td>");
			$.each(categoryValue, function(indexInArray, dataValue) {
				var tr = $('<tr class="accordion-' + (categoryName.replace(/\s/g, "-")).safeCSSId() + ' collapse in" aria-expanded="true"></tr>').appendTo(tBody);
				var text = "";

				var goal;
				if (dataValue.goals.length != 0) {
					goal = dataValue.goals[dataValue.goals.length - 1].y == null ? "TBD" : dataValue.goals[dataValue.goals.length - 1].y;

				} else {
					goal = "TBD";
				}
				var tooltipValue;
				var unit = "";
				var unitGoal = "";
				var value;
				var idChart = "chart-" + idChartNum;
				idChartNum++;
				var unitIndicator = "";
				if (dataValue.value == "-" || dataValue.value == null) {
					precission = 2;
					value = dataValue.value;
					tooltipValue = "";
				} else {
					precission = 2;
					value = autoFormatNumber(dataValue.value, 2);
					tooltipValue = truncate(dataValue.value, 2);
				}
				if (dataValue.unit != "#" && dataValue.unit != null && dataValue.value != "-" && dataValue.value != null) {
					unit = dataValue.unit;
				}
				
				if (dataValue.unit != "#" && dataValue.unit != null) {
					unitGoal = dataValue.unit;
				}
				
				if (dataValue.unit != null) {
					unitIndicator = dataValue.unit;
				}

				value += unit;
				tooltipValue += unit;

				var regex = /[/]+[a-zA-Z|1-9|[_\]|-]+\b(.aspx)\b/;
				var nameClass = "KPIName";
				if (dataValue.value == "-"){
					nameClass = "KPIName_disable";
				}
				text += ("<th class='" + nameClass + "' data-container='body' data-toggle='tooltip' title='" + dataValue.codeDesc + "'>" + dataValue.name + "</th>");
				text += ("<td> <div id='" + idChart + "' class='chart_area' > </div></td>");
				text += ("<td class='goal' ><span data-toggle='tooltip' title=" + goal + unitGoal + ">" + goal + unitGoal + "</span>" + "</td>");
				text += ("<td class='value'>" + "<span data-toggle='tooltip' title=" + tooltipValue + ">" + value + "</span>" + "</td>");
				text += ("<td class='status'>" + getStatusIcon(dataValue.status, dataValue.coloured, dataValue.name, dataValue.statusValue, precission) + "</td>");
				if (dataValue.unit != "%" && dataValue.trend) {
					var trend = getTrendValue(dataValue.unit, dataValue.trend, dataValue.value)
					text += ("<td class='trend'>" + getTrendIcon(trend, dataValue.coloured, dataValue.goalTrend, isOrange) + "</td>");

				} else {
					text += ("<td class='trend' >" + getTrendIcon(dataValue.trend, datos[4].coloured, datos[4].goalTrend, isOrange) + "</td>");
				}
				var data = dataValue.data;
				var goals = {};
				var series = [{
					data: data,
					name: dataValue.code
				}];
				if (dataValue.goals && _.filter(dataValue.goals, function(value) {
						return value.y != null;
					}).length > 0) {
					goals.name = "Goal";
					goals.type = 'spline';
					goals.datalabels = false;
					goals.data = dataValue.goals;
					series.push(goals);
				}

				tr.append(text);
				chart = {};
				dataValue.highchartsData = [];
				dataValue.highchartsData.push('VALOR')
				dataValue.categoriesHighChart = [];
				$.each(dataValue.data, function(index, valueHighchart) {
					dataValue.highchartsData.push(valueHighchart.y)
					dataValue.categoriesHighChart.push(valueHighchart.name);
				});
				renderChartTab(idChart, dataValue);
				
				
				
			});

		});
		$('[data-toggle="tooltip"]').tooltip();
		
		resizeCharts();
		return;
	};
}


String.prototype.safeCSSId = function() {
    return encodeURIComponent(
        this.toLowerCase()
    ).replace(/%[0-9A-F]{2}/gi, '');
}



var countFamily = 1;

function generateTab(dataValue) {
    if (countFamily == 1) {

        $("#tab_general").after("<li id='tab_1' class='nav-item'><a class='nav-link' data-toggle='tab' href='#swipe-" + (countFamily + 1) + "' role='tab'>" + dataValue.category + "</a></li>");
        $("#swipe-1").after(getHtmlContentTab());
    } else {

        $("#tab_" + (countFamily - 1)).after("<li id='tab_" + countFamily + "' class='nav-item'><a class='nav-link' data-toggle='tab' href='#swipe-" + countFamily + "' role='tab'>" + dataValue.category + "</a></li>");
        $("#swipe-" + (countFamily - 1)).after(getHtmlContentTab());
    }
    countFamily++;
}

function getHtmlContentTab() {
    return "<div id='swipe-" + countFamily + "' class='tab-pane active' role='tabpanel'><div class='card' style='background: white'><div class='card-content row'><div class='col-sm-10 col-md-10' style='float: none;margin: 0 auto;'><table id='table' class='table'><thead><tr id='copyValue' style='height: 0px !important'><th class='title' style='width: 140px; height: 0px'>Indicator name</th><th class='title' style='width: 100px; height: 0px'>Evolution</th><th class='title' style='width: 21px;height: 0px'>Goal</th><th class='title' style='width: 21px; height: 0px'>Value</th><th class='title' style='width: 15px; height: 0px'>Status</th><th class='title' style='width: 15px; height: 0px'>Trend</th></tr><tr id='header' style='display: none;background-color: white; z-index: 1000;'><th class='title' style='width: 100px; height: 0px'>Indicator name</th><th class='title' style='width: 140px; height: 0px'>Evolution</th><th class='title' style='width: 21px; height: 0px'>Goal</th><th class='title' style='width: 21px; height: 0px'>Value</th><th class='title' style='width: 15px; height: 0px'>Status</th><th class='title' style='width: 15px; height:0px'>Trend</th></tr></thead><tbody></tbody></table></div></div></div></div>";
}


function getStatusIcon(status, coloured, name, statusValue, precission) {
    var colorClass, iconClass, statusTooltip;
    if (status != null && coloured) {
        iconClass = 'fa-circle';
        switch (status) {
            case "VERDE":
				statusTooltip = 'OK';
                colorClass = 'green-text';
                break;
            case "AMARILLO":
                statusTooltip = 'KO';
				colorClass = 'orange-text';
                break;
            case "ROJO":
				statusTooltip = 'KO';
                colorClass = 'red-text';
                break;
            case "GRIS":
				statusTooltip = '-';
                return '<span><span class="fa-circle" data-toggle="tooltip" title="' + statusTooltip + '" data-position="top" style="color:#736E6E;"></span></span>';
                break;
        }

    } else {
        if (status == null) {
            status = "";
			statusTooltip = '-';
        }
        return '<span><span class="fa-circle" data-toggle="tooltip" title="' + statusTooltip + '" data-position="top" style="color:#736E6E;"></span></span>'
    }
    return '<span class="' + colorClass + '"><span class="' + iconClass +  '" data-toggle="tooltip" title="' + statusTooltip + '" data-position="top" ></span></span>'
}


function getTrendIcon(trend, coloured, goodWhen, isOrange) {
    var colorClass, iconClass;
    var changedTrend = goodWhen == 'DESC' ? +trend * (-1) : +trend;
    if (trend != null) {
        if (trend > 0) {
            iconClass = 'icon-chevron-up';
        } else if (trend < 0) {
            iconClass = 'icon-chevron-down';
        } else {
            iconClass = 'fa-equal';
        }
        if (coloured && goodWhen != null) {
            if (changedTrend > 0)
                colorClass = 'green-text';
            else if (changedTrend < 0) {
                if (isOrange)
                    colorClass = 'orange-text';
                else
                    colorClass = 'red-text';
            }
        } else
            colorClass = "";
        trend = truncate(Math.abs(trend), 2);
        trend += '%';
    } else {
        trend = "-";
        colorClass = "";
        return '<span class="trendIcon ' + colorClass + '"><i data-toggle="tooltip" title="' + trend + '" data-position="top" class="icon-check-empty"></i></span>';
    }
    return '<span class="trendIcon ' + colorClass + '"><i data-toggle="tooltip" title="' + trend + '" data-position="top" class="' + iconClass + '"></i></span>';
}


function getTrendValue(unit, trend, value) {
    var trendRes;
	if (trend == null)
        trendRes = "";
    else
        trendRes = parseFloat(trend).toFixed(2);
    return trendRes;
}

function getEvolutionStatusTrendGroupedBy(data, code, dates) {
    var year = dates.year;
    var month = dates.month;
    var unique = {};
    var distinct = [];
    for (var i in data) {
        if (typeof(unique[data[i][code]]) == "undefined") {
            distinct.push({
                code: data[i][code],
                codeDesc: data[i]['DESCRIPCION'],
                name: data[i]['ANS'],
                data: [],
                goals: [],
                value: "-",
                status: null,
                statusValue: null,
                trend: null,
                coloured: false,
                unit: data[i]['UNIDAD'],
				group: data[i]['GRUPO'],
                category: data[i]['CATEGORIA'],
                categories: [],
                goalTrend: null,
            });
        }
        unique[data[i][code]] = 0;
    }

    data = _.uniq(data, function(x) {
        return [x.CODE, x.DATE_CAT].join();
    });


    data.map(function(obj, index) {
        var elem = distinct.filter(function(e) {
            return e.code == obj[code];
        })[0];

		elem.data.push({ y: obj.VALOR, unit: obj.UNIDAD, name: obj.DATE_CAT});
        elem.goals.push({ y: obj.GOAL, unit: obj.UNIDAD});
        elem.categories.push(obj.DATE_CAT);
        elem.UNIDAD = obj.UNIDAD == "min." ? " min." : obj.UNIDAD;

        if (month == obj.MES && year == obj.ANYO) {
            elem.goalTrend = obj.TENDENCIA;
            elem.status = obj.ESTADO;
            elem.trend = obj.PERC_INCREMENTO;
            elem.value = obj.VALOR != null ? obj.VALOR : "-";
            elem.coloured = true;
        }
    });
    return distinct;
}

var numFamily = 0;
var totalKPIsCount = 0;
var countFamilyInDataMap = {};

function countFamilyInData(datos) {
    $.each(datos, function(indexInArray, valueOfElement) {
        if (countFamilyInDataMap.hasOwnProperty(valueOfElement.category)) {
            countFamilyInDataMap[valueOfElement.category] = countFamilyInDataMap[valueOfElement.category] + 1;
        } else {
            countFamilyInDataMap[valueOfElement.category] = 1;
        }
    });
}

function calculateColumCategory(numKPIFamily, totalKPIs) {
    var width = new Object();
    var percentage = (numKPIFamily / totalKPIs) * 100;
    var minPercentage = Object.keys(countFamilyInDataMap).length <= 7 ? 33.33 : 10;
    if (percentage <= minPercentage) {
        width.widthCategory = 4;
        width.widthCard = 12;
    } else if (percentage > minPercentage && percentage <= 66.666) {
        width.widthCategory = 8;
        width.widthCard = 6;
    } else if (percentage > 66.666) {
        width.widthCategory = 12;
        width.widthCard = 4;
    }
    return width;
}

function renderChartTab(idChart, dataValue) {
	
	var alto =$('#'+idChart).height()+5;
	
	c3.generate({
		bindto: '#'+idChart,
		
		data: {
			columns: 
				[dataValue.highchartsData]
			,
			types: {
				VALOR: 'area-spline'
			}
		},
		tooltip: {
			position: function (data, width, height, element) {
				return {top: -30, left: 50};
			}
			},
		legend: {
			show: false
		}, 
		
		axis: {
			x: {
				type: 'category',
				categories: dataValue.categoriesHighChart,
				show:false
			},
			y: {show:false}
		}, 
		size: {height:alto, width:320},     
		point: {
			show: true
		}
	});

	


   /* Highcharts.chart(idChart, {
        chart: {
            type: 'area',
            spacingBottom: 0,
            backgroundColor: null,
            margin: [0, 0, 0, 0],
        },

        title: {
            text: null
        },

        subtitle: {
            text: null
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100, 
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {

            categories: dataValue.categoriesHighChart,
            lineWidth: 0,
            minorGridLineWidth: 0,
            lineColor: 'transparent',
            labels: {
                enabled: false
            },
            minorTickLength: 0,
            tickLength: 0,
            gridLineColor: 'transparent',
            gridLineWidth: 0,
            minorGridLineWidth: 0,

        },
        yAxis: {
            endOnTick: false,
            startOnTick: false,
            labels: {
                enabled: false
            },
            title: {
                text: null
            },
            gridLineWidth: 0,
            minorGridLineWidth: 0,
            gridLineColor: 'transparent'
        },
        tooltip: {
            formatter: function() {
                return "<div style='position: absolute; z-index: 2000'><b>" + this.x + "<b/><br/>" +
                    this.series.name + ": " + truncate(this.y, 2) + dataValue.unit + "<br/></div>";
            },
			
            positioner: positionerTooltip,
            hideDelay: 0
        },
        plotOptions: {
            series: {
                stickyTracking: false,
                animation: false,
                lineWidth: 1,
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                marker: {
                    radius: 2,
                    states: {
                        hover: {
                            radius: 2
                        }
                    }
                },
                fillOpacity: 0.25
            },

        },
        credits: {
            enabled: false
        },
        series: [{
            showInLegend: false,
            data: dataValue.highchartsData,
            name: dataValue.code
        }]
    });*/
}

function removeLoading() {
	$("#loading").fadeOut("slow");
	$(".hiddenWhileLoading").fadeIn("slow");
};

//# sourceURL=dashboardANS.js 