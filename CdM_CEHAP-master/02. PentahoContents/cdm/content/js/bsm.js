 var reportingPeriod;
// var showCategoryKPIs = true;
 var firstLoad = true;
 var initPqSelect = true;
 var data;
selectorPrefix = " ";

$(document).ready(function(){
	var dataRequestCalendar =  new DataPentahoRequest('/public/DashboardBSM.cda', 'dsReportingPeriod');
	buildFilterDateANS('ReportingPeriodCalendar', dataRequestCalendar, 'months', init, null, true);
	$( document ).ajaxStop(function() {
		if (firstLoad) {
			$("#bsmTab").addClass("active");
			firstLoad = false;
		}
		removeLoading();		
	});
	
});



 function init(){
	 if(initPqSelect){
		createSelectComboFilters();
		initPqSelect = false;
	}
	reportingPeriod = getReportingPeriodANS($("#ReportingPeriodCalendar"));
	createFilters();
	renderCharts();
 }
 
 function createFilters(){
	
	var dataRequestFiltroAplicacion = new DataPentahoRequest('/public/DashboardBSM.cda','dsFiltroAplicacion');
	dataRequestFiltroAplicacion.addParameter("paramYear",reportingPeriod.year);
	dataRequestFiltroAplicacion.addParameter("paramMonth",reportingPeriod.month);
	
	SQL_OBJECT_FILTER_ARRAY["paramaplicacion"] = dataRequestFiltroAplicacion;
		
	getJSONFilter(dataRequestFiltroAplicacion, false, "Aplicacion", "Aplicacion_Select", "APLICACION", "paramaplicacion", pqSelectPopulation, appFilterSelect, FILTER_TYPE.MULTIPLE);

}

 
function renderCharts(){
	var dataRequestAvailabilityChart = new DataPentahoRequest('/public/DashboardBSM.cda','dsAvailability');
	dataRequestAvailabilityChart.addParameter("paramYear",reportingPeriod.year);
	dataRequestAvailabilityChart.addParameter("paramMonth",reportingPeriod.month);
	
	var dataRequestRendimientoChart = new DataPentahoRequest('/public/DashboardBSM.cda','dsRendimiento');
	dataRequestRendimientoChart.addParameter("paramYear",reportingPeriod.year);
	dataRequestRendimientoChart.addParameter("paramMonth",reportingPeriod.month);
	
    SQL_OBJECT_ARRAY.push(dataRequestAvailabilityChart);
	SQL_OBJECT_ARRAY.push(dataRequestRendimientoChart);
	
	createParametersSQLObjects();

	getPentahoRequest(renderAvailabilityChart,dataRequestAvailabilityChart,false);
	getPentahoRequest(renderRendimientoChart,dataRequestRendimientoChart,false);
	
	function renderAvailabilityChart(response){
		tratarRespuesta(response, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
		plotAvailbilityAppChart(jsonResponse);
		plotAvailabilityHChart(jsonResponse);
	}
	function renderRendimientoChart(response){
		tratarRespuesta(response, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
		plotRendimientoHChart(jsonResponse);
		plotRendimientoAppChart(jsonResponse);
	}
	
}
function plotAvailbilityAppChart(response){
	var categories = []
	var series = [];
	series.push("Disponibilidad");

	$.each(response,function(i,value){
		series.push(parseFloat(response[i].DISPONIBILIDAD).toFixed(2));
		categories.push(response[i].APLICACION);
	});
	
	
	c3.generate({
		bindto: '#AvailabilityAppChart',
		data: {
          columns: [series],
          type: 'bar',
		},
		 bar: {
        width: {
            ratio: 0.5 // this makes bar width 50% of length between ticks
        }
        // or
        //width: 100 // this makes bar width 100px
    },
		color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
		},
		
        axis: {
		  x: {
            type: 'category',
            categories: categories
        }
        },
		size: {height:300, width:515},
        legend: {
          show: true
        }
      });
}
function plotAvailabilityHChart(response){
	var aplicacion = []
	var categories = []
	var dateAux=0;
	var series = []
	
	
	if(SQL_OBJECT_ARRAY[1].paramaplicacion== "@@@___@@@"){
		aplicacion.push('Total');
		$.each(response, function (i,value){
			dateAux = response[i].DATE_CAT;
			data=0;
			var count =0;
			$.each(response, function (i,value){
				if(!categories.includes(dateAux)){
				if(dateAux == response[i].DATE_CAT){
					data +=response[i].DISPONIBILIDAD
					count++;
				}
				}
			});
			if(!categories.includes(dateAux)){
				categories.push(response[i].DATE_CAT);
				aplicacion.push(parseFloat(data/count).toFixed(2));
			}
			
		});	
			
			
			c3.generate({
			bindto: '#test',
			data: {
			  columns: [aplicacion],
			  type: 'bar',
			},bar: {
        width: {
            ratio: 0.5
        }
    },
			color: {
			pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
			},
			axis: {
			  x: {
				type: 'category',
				categories: categories
			}
			},
		size: {height:300, width:515},
			legend: {
			  show: true
			}
		  });
			
	}else{	
	
	series.push("Disponibilidad");
	$.each(response, function (i,value){
		if(!aplicacion.includes(response[i].APLICACION)){
			aplicacion.push(response[i].APLICACION);	
		}
			series.push(parseFloat(response[i].DISPONIBILIDAD).toFixed(2));
		});
		
		c3.generate({
			bindto: '#test',
			data: {
			  columns: [series],
			  type: 'bar',
			},bar: {
        width: {
            ratio: 0.5 
				}
					},
			color: {
			pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
			},
			axis: {
			  x: {
				type: 'category',
				categories: aplicacion
			}
			},
		size: {height:300, width:515},
			legend: {
			  show: true
			}
		  });
		}
 }
 
function plotRendimientoHChart(response){

	var aplicacion = []
	var categories = []
	var dateAux=0;
	var series=[];
	
	if(SQL_OBJECT_ARRAY[1].paramaplicacion== "@@@___@@@"){
		aplicacion.push('Total');
		console.log('Aqui');
		$.each(response, function (i,value){
			dateAux = response[i].DATE_CAT;
			data=0;
			var count =0;
			$.each(response, function (i,value){
				if(!categories.includes(dateAux)){
				if(dateAux == response[i].DATE_CAT){
					data +=response[i].RENDIMIENTO
					count++;
				}
				}
				
			});
			if(!categories.includes(dateAux)){
				categories.push(response[i].DATE_CAT);
				aplicacion.push(parseFloat(data/count).toFixed(2));
			}
		
	
		});	
		
		c3.generate({
		bindto: '#rendimientoChart',
		data: {
          columns: [aplicacion],
          type: 'bar',
		},bar: {
        width: {
            ratio: 0.5 
        }
    },color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
		},axis: {
		  x: {
            type: 'category',
            categories: categories
        }
        },size: {height:300, width:515},
        legend: {
          show: true
        }
      });		
		
}else{	
series.push('Rendimiento');
	$.each(response, function (i,value){
		if(!aplicacion.includes(response[i].APLICACION)){
			aplicacion.push(response[i].APLICACION);	
		}
			series.push(parseFloat(response[i].RENDIMIENTO).toFixed(2));
		});
		
		c3.generate({
		bindto: '#rendimientoChart',
		data: {
          columns: [series],
          type: 'bar',
		},bar: {
        width: {
            ratio: 0.5 
        }
       
    },
		color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
		},
		
        axis: {
		  x: {
            type: 'category',
            categories: aplicacion
        }
        },
		size: {height:300, width:515},
        legend: {
          show: true
        }
      });	
	}
 }
function plotRendimientoAppChart(response){
	
	var categories = []
	var series = [];
	var apps = [];	
	series.push("Rendimiento");

	
	$.each(response,function(i,value){
		series.push(parseFloat(response[i].RENDIMIENTO).toFixed(2));
		apps.push(response[i].APLICACION);
		categories.push(response[i].APLICACION);
		
	});
	
	var chart=c3.generate({
		bindto: '#rendimientoAppChart',
		data: {
          columns: [series],
          type: 'bar',
		  onclick: function (d, element) {
				if(d.id=='Rendimiento'){
			//d3.select("#rendimientoAppChart").selectAll(".c3-event-rect").on("click", null);
			
			var newColumns=[];
			newColumns.push("Transacción");
			categories=[];

		  
		  var dataRequestRendimientoTransiccionChart = new DataPentahoRequest('/public/DashboardBSM.cda','dsRendimientoTransaccion');
			dataRequestRendimientoTransiccionChart.addParameter("paramYear",reportingPeriod.year);
			dataRequestRendimientoTransiccionChart.addParameter("paramMonth",reportingPeriod.month);
			dataRequestRendimientoTransiccionChart.addParameter("paramaplicacion",apps[d.x]);
			
			SQL_OBJECT_ARRAY.push(dataRequestRendimientoTransiccionChart);


			getPentahoRequest(renderRendimientoTransiccionChart,dataRequestRendimientoTransiccionChart,false);
	
			function renderRendimientoTransiccionChart(response){
				tratarRespuesta(response, this.queryAlias);
				var jsonResponse = getResponseFromQuery(this.queryAlias);
				$.each(jsonResponse,function (i,value){
					newColumns.push(jsonResponse[i].RENDIMIENTO);
					categories.push(jsonResponse[i].TRANSACCION);
				});
			}
          chart.load({
            unload: true,
            columns: [newColumns],
			categories:categories
				
          });

		  $('#returnButton').show();
		  $('#returnButton').click(function (){
			$('#returnButton').hide();
			
			plotRendimientoAppChart(response);
			
			// chart.load({
            // unload: true,
            // columns: series,
          // });
			  
		  });
		  
		 

		}
		  }
		},
		 bar: {
        width: {
            ratio: 0.5 // this makes bar width 50% of length between ticks
        }
        // or
        //width: 100 // this makes bar width 100px
    },
		color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
		},
		
        axis: {
		  x: {
            type: 'category',
            categories: categories,
			tick: {
                rotate: 15,
                multiline: false
            }
        }
        },
	size: {height:300, width:515},
        legend: {
          show: true
        }
      });
	//d3.select("#rendimientoAppChart").selectAll(".c3-event-rect").on("click", null);
	
	
}






function plotRendimientoTransChart(d,element){
	var categories = []
	var series = [];
	var apps = [];	
	series.push("Rendimiento");
	
	var dataRequestRendimientoTransiccionChart = new DataPentahoRequest('/public/DashboardBSM.cda','dsRendimientoTransaccion');
	dataRequestRendimientoTransiccionChart.addParameter("paramYear",reportingPeriod.year);
	dataRequestRendimientoTransiccionChart.addParameter("paramMonth",reportingPeriod.month);
	dataRequestRendimientoTransiccionChart.addParameter("paramaplicacion",d.id);

	getPentahoRequest(renderRendimientoTransiccionChart,dataRequestRendimientoTransiccionChart,false);
	
	function renderRendimientoTransiccionChart(response){
		tratarRespuesta(response, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
		$.each(jsonResponse,function (i,value){
			categories.push(jsonResponse[i].TRANSACCION);
			series.push(parseFloat(jsonResponse[i].RENDIMIENTO).toFixed(2));
		});
	}
	
	var chart=c3.generate({
		bindto: '#rendimientoAppChart',
		data: {
          columns: [series],
          type: 'bar',
		},
		 bar: {
        width: {
            ratio: 0.5 // this makes bar width 50% of length between ticks
        }
        // or
        //width: 100 // this makes bar width 100px
    },
		color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
		},
		
        axis: {
		  x: {
            type: 'category',
            categories: categories
        }
        },
	size: {height:300, width:515},
        legend: {
          show: true
        }
      });
	
	$('#returnButton').show();
		  $('#returnButton').click(function (){
			$('#returnButton').hide();
			
			plotRendimientoAppChart(response);
			
			// chart.load({
            // unload: true,
            // columns: series,
          // });
			  
		  });
	
}


/*

var drilldata = {
    data1: [
        ['d1-1', 10],
        ['d1-2', 20],
        ['d1-3', 30],
        ['d1-4', 40],
        ['d1-5', 50]
    ],
    data2: [
        ['d2-1', 50],
        ['d2-2', 40],
        ['d2-3', 30],
        ['d2-4', 20],
        ['d2-5', 10]
    ]
};

var chart = c3.generate({
    bindto: '#chart3',
    data: {
        columns: [
            ['data1', 30],
            ['data2', 50]
        ],
        type : 'pie',
        onclick: function (d, element) { 
          chart.load({
            unload: ['data1', 'data2'],
            columns: drilldata[d.id],
          });
        },
    }
});

d3.select('#C')
  .on('click', function(d, element) {
    chart.load({
      unload: true,
      columns: [
        ['data1', 30],
        ['data2', 50]
      ],
    });
  });

  *///////////////////////


function removeLoading() {
	$("#loading").fadeOut("slow");
	$(".hiddenWhileLoading").fadeIn("slow");
};

//# sourceURL=bsm.js 