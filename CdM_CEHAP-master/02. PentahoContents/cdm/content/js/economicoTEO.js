var reportingPeriod;
var firstLoad = true;
var initPqSelect = true;
var createTable = true;
var histogramData=[];
var categories=[];
var tableImportes;
var alphabet = String.fromCharCode(...Array(123).keys()).slice(97)

selectorPrefix = " ";

$(document).ready(function(){
	var dataRequestCalendar =  new DataPentahoRequest('/public/EconomicoTEOView.cda', 'dsReportingPeriod');
	buildFilterDateRange('ReportingPeriodRange', dataRequestCalendar, 'months', init, null, false);
	$( document ).ajaxStop(function() {
		if (firstLoad) {
			$("#economicoTEOTab").addClass("active");
			firstLoad = false;
		}
		removeLoading();
		resizeCharts();
	});
});

function init(selector){
	if(initPqSelect){
		createSelectComboFilters();
		initPqSelect = false;
	}
	reportingPeriod = getReportingPeriod($("#ReportingPeriodRange"));
	createFilters();
	renderCharts();
	
}

function createFilters(){
	
	var dataRequestFiltroContrato = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsFiltroContrato');
	var dataRequestFiltroProyecto = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsFiltroProyecto'); 	 
	var dataRequestFiltroEstado = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsFiltroEstado');
	var dataRequestFiltroVersion = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsFiltroVersion');
	var dataRequestFiltroDesvTiempo = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsFiltroDesvTiempo');
	var dataRequestFiltroDesvImporte = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsFiltroDesvImporte');
	
	SQL_OBJECT_FILTER_ARRAY["paramcontrato"] = dataRequestFiltroContrato;
	SQL_OBJECT_FILTER_ARRAY["paramproyecto"] = dataRequestFiltroProyecto;
	SQL_OBJECT_FILTER_ARRAY["paramestado"] = dataRequestFiltroEstado;
	SQL_OBJECT_FILTER_ARRAY["paramversion"] = dataRequestFiltroVersion;
	SQL_OBJECT_FILTER_ARRAY["paramdesvTiempo"] = dataRequestFiltroDesvTiempo;
	SQL_OBJECT_FILTER_ARRAY["paramdesvImporte"] = dataRequestFiltroDesvImporte;
	
	for (var d in SQL_OBJECT_FILTER_ARRAY){
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramcontrato", "@@@___@@@");
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramproyecto", "@@@___@@@");
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramestado", "@@@___@@@");
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramversion", "@@@___@@@");
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramDateFrom",reportingPeriod.start);
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramDateTo",reportingPeriod.end);
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramdesvTiempo","@@@___@@@");
		SQL_OBJECT_FILTER_ARRAY[d].addParameter("paramdesvImporte","@@@___@@@");
	}
	
	getJSONFilter(dataRequestFiltroContrato, false, "Contrato", "Contrato_Select", "CONTRATO", "paramcontrato", pqSelectPopulation, appFilterSelect, FILTER_TYPE.MULTIPLE);
	getJSONFilter(dataRequestFiltroProyecto, false, "Proyecto", "Proyecto_Select", "PROYECTO", "paramproyecto", pqSelectPopulation, appFilterSelect, FILTER_TYPE.MULTIPLE);
	getJSONFilter(dataRequestFiltroEstado, false, "Estado", "Estado_Select", "ESTADO", "paramestado", pqSelectPopulation, appFilterSelect, FILTER_TYPE.MULTIPLE);
	getJSONFilter(dataRequestFiltroVersion, false, "Version", "Version_Select", "VERSION", "paramversion", pqSelectPopulation, appFilterSelect, FILTER_TYPE.MULTIPLE);
	getJSONFilter(dataRequestFiltroDesvTiempo, false, "Desviación Tiempo", "DesvTiempo_Select", "DESVTIEMPO", "paramdesvTiempo", pqSelectPopulation, appFilterSelect, FILTER_TYPE.SINGLE);
	getJSONFilter(dataRequestFiltroDesvImporte, false, "Desviación Importe", "DesvImporte_Select", "DESVIMPORTE", "paramdesvImporte", pqSelectPopulation, appFilterSelect, FILTER_TYPE.SINGLE);	
}

function renderCharts(){
	/*PIECHART*/
	var dataRequestPieChartState = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsPieChartState');
	dataRequestPieChartState.addParameter("paramDateFrom",reportingPeriod.start);
	dataRequestPieChartState.addParameter("paramDateTo",reportingPeriod.end);
	
	var dataRequestPieChartCategory = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsPieChartCategory');
	dataRequestPieChartCategory.addParameter("paramDateFrom",reportingPeriod.start);
	dataRequestPieChartCategory.addParameter("paramDateTo",reportingPeriod.end);
	
	/*HISTOGRAM*/
	var dataRequestHistogramProject = new DataPentahoRequest('/public/EconomicoTEOView.cda','dsHistogramaProyecto');
	dataRequestHistogramProject.addParameter("paramDateFrom",reportingPeriod.start);
	dataRequestHistogramProject.addParameter("paramDateTo",reportingPeriod.end);

	/*TABLE*/
	var dataRequestTablaImportes =  new DataPentahoRequest('/public/EconomicoTEOView.cda', 'dsTablaImportes');
	dataRequestTablaImportes.addParameter("paramDateFrom",reportingPeriod.start);
	dataRequestTablaImportes.addParameter("paramDateTo",reportingPeriod.end);
	
    SQL_OBJECT_ARRAY.push(dataRequestPieChartState);
	SQL_OBJECT_ARRAY.push(dataRequestPieChartCategory);
    SQL_OBJECT_ARRAY.push(dataRequestHistogramProject);
    SQL_OBJECT_ARRAY.push(dataRequestTablaImportes);
	
	createParametersSQLObjects();

	getPentahoRequest(renderPieChartState,dataRequestPieChartState,false);
	getPentahoRequest(renderPieChartCategory,dataRequestPieChartCategory,false);
	getPentahoRequest(renderHistogram,dataRequestHistogramProject,false);
	getPentahoRequest(renderaAmountTable, dataRequestTablaImportes, false);
	
	function renderPieChartState(response){
		tratarRespuesta(response, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
		desgloseImportesEstado(jsonResponse);
	}
	
	function renderPieChartCategory(response){
		tratarRespuesta(response, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
		desgloseImportesCategoria(jsonResponse);
	}
	
	function renderHistogram(response){
		tratarRespuesta(response, this.queryAlias);
		var jsonResponse = getResponseFromQuery(this.queryAlias);
		histogramData = dataToHistogram(jsonResponse);
		mensualEvolution();
	}
	
	function renderaAmountTable(response){
		tratarRespuesta(response, this.queryAlias);
		treeTableImportes();
	}
	
	$('[data-toggle="tooltip"]').tooltip();
	$('.container').css('width',$(this)[0].outerWidth*0.85);
	$('.container').css('min-width','1215px');
	$('.span12').css('min-width','1215px');
	$('.span12').css('width',$(this)[0].outerWidth*0.85);
	$('.span6').css('min-width','590px');
	$('.span6').css('width',$(this)[0].outerWidth*0.85*0.485);

	$('.c3-chart-texts').css('font','12px sans-serif');
	
	
}




function loadTableImportes(){
	tableImportes = $('#tableImportes').DataTable({
		paginate: false,
		dom: 'frltipB',
		deferRender: true,
		buttons: [{
			extend: 'copy',
			exportOptions: {
				columns: ':not(.notexport)'
			}
		}, {		
			extend: 'excelHtml5',
			title: 'Seguimiento TEO',
            customize: function(xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                $('row c[r*="A"]', sheet).each( function (i, d) {
                    if ($('is t', this).text() == '-') {
						var row = $(this).attr("r");
						var numberPattern = /\d+/g;
						row = row.match(numberPattern)
						$('row c[r*="' + row + '"]', sheet).attr('s', '7');
						row = -1;
                    }
                });
            },
			exportOptions: {
				columns: ':not(.notexport)'
			}
		}, {
			extend: 'print',
			exportOptions: {
				columns: ':not(.notexport)'
			}
		}],
		language: 	{
			emptyTable:     "No data available in table",
			info:           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
			infoEmpty:      "Registros vacios",
			infoFiltered:   "(De un total de _MAX_ registros)",
			lengthMenu:     "Mostrar _MENU_ registros",
			loadingRecords: "Cargando...",
			processing:     "Procesando...",
			search:         "B&uacute;squeda:",
			zeroRecords:    "Registros no encontrados",
			paginate: {
				firs:      "Primera",
				last:       "&Uacute;ltima",
				next:       "Siguiente",
				previous:   "Anterior"
			},
			buttons: {
					copy: 'Copiar',
					print: 'Imprimir'
				}
		},
		responsive: true,
		autoWidth: false,
		'aaSortingFixed': [[12,'asc']],
		"orderClasses": false,
		columnDefs: [{
			"orderable": false,
			width: "5%",
			targets: 0
		}, {
			width: "8%",
			targets: 1
		}, {
			width: "8%",
			targets: 2
		}, {
			width: "10%",
			targets: 3
		}, {
			width: "10%",
			targets: 4
		}, {
			width: "10%",
			targets: 5
		}, {
			width: "20%",
			targets: 6
		},{
			width: "20%",
			targets: 7
		},{
			width: "20%",
			targets: 8
		},{
			width: "10%",
			targets: 9
		},{
			width: "10%",
			targets: 10
		},{
			width: "20%",
			targets: 11
		},{
			visible: false,
			targets: 12
		}]
	});
}

function treeTableImportes(){
	var jsonResponse = getResponseFromQuery("dsTablaImportes");
	getDataArea(jsonResponse);

	function getDataArea(response){
		var estados = []
		var area = []
		var id =1
		var html= "<table id='tableImportes' class='display cell-border'><thead><tr>"+
				  "<th class='notarrow'></th>"+
				  "<th>ID</th>"+
				  "<th class='areaName wrapped'>Asunto</th>"+
				  "<th>Contrato</th>"+
				  "<th>Proyecto</th>"+
				  "<th>Estado</th>"+
				  "<th>Inicio</th>"+
				  "<th>Fin</th>"+
				  "<th>Versi&#243;n</th>"+
				  "<th>Desviaci&#243;n Tiempo</th>"+
				  "<th>Importe Total</th>"+
				  "<th>Desviaci&#243;n Importe</th>"+
				  "<th class='notexport'>Order Code</th>"+
				  "</tr></thead><tbody>"
				  
	//Rellena categorias
		$.each(response, function (i,value){
			if (area.indexOf(value.CATEGORIA) < 0 && value.CATEGORIA != null){
				area.push(value.CATEGORIA);
			}
		});
		
	//Rellena estados
	$.each(response, function (i,value){
			if (estados.indexOf(value.ESTADO) < 0 && value.ESTADO != null){
				estados.push(value.ESTADO);
			}
		});

		for (a in area){
			var totalArea = 0
			html =html+"<tr class='breakrow expanded' id='"+id+"'>"
					   +"<td class='glyphicon glyphicon-minus-sign'><span style='color: #CFC889'>-</span></td>"
					   +"<td></td>"
					   +"<td class='areaName wrapped' data-container='body'  data-toggle='tooltip' title='" + area[a] + "'>" + area[a] + "</td>"
			
			for (e in estados){
				var totalEstado = 0
				
				$.each(response, function (i,value){
					if (value.CATEGORIA == area[a] && value.ESTADO == estados[e]){
						totalEstado += value.IMPORTE
					}
				});
				
				totalArea +=totalEstado

			}
			html += "<td></td>"+
					"<td></td>"+"<td></td>"+
					"<td></td>"+"<td></td>"+
					"<td></td>"+"<td></td>"+
					"<td style='text-align: center;'>"+toEuros(totalArea)+"</td>"+
					"<td></td>"+
					"<td class='notexport'>" + alphabet[id-1] + (id - 1) + "</td></tr>"
			html += getChildren(area[a],response,id,estados);
			
			id++;
		}
		html +="</tbody></table>"	
		
		$('#tableWidget').html(html);
	
		loadTableImportes();
		var pos={};
		var ind =-1;
		
		
		$(".breakrow").on( "click",function() {
			var attr ='datarow'+$(this).attr('id');
			var tr = "tr#"+$(this).attr('id');
			var icon = $(this).find(".glyphicon");
			
			if($(tr).hasClass("expanded")){
				ind++;
				pos[tr] = ind;
				$.fn.dataTable.ext.search.push(
					function(settings, data, dataIndex) {
						
						var r = $(tableImportes.row(dataIndex).node())[0];
						return (!$(r).hasClass(attr) || $(r).hasClass('breakrow'));
					}
				);
				icon.removeClass("glyphicon-minus-sign"); 
				$(tr).removeClass("expanded");
				icon.addClass("glyphicon-plus-sign");
				$(tr).addClass("collapsed");
				tableImportes.draw();
			} else if ($(tr).hasClass("collapsed")){
				$.fn.dataTable.ext.search.splice(pos[tr],1);
				
				for (i in pos){
					if(pos[tr]<pos[i]){
						pos[i]--;
					}
					
					
				}
				delete pos[tr];
				ind--;
				icon.removeClass("glyphicon-plus-sign"); 
				$(tr).removeClass("collapsed");
				icon.addClass("glyphicon-minus-sign");
				$(tr).addClass("expanded");
				tableImportes.draw();		
			}
		});	
		
		$(".areaName").on( "click",function(row) {
			if ($(this).hasClass("wrapped")) {
				$(this).removeClass("wrapped");
			} else {
				$(this).addClass("wrapped");
			}
		});
	}
	
	tableImportes.on('search.dt', function () {
		 var value = $('.dataTables_filter input').val(); 
		 if (value.length > 3) {
			tableImportes.search(value).draw();
		} 		
	});
	
	function getChildren(Area,response,idt,estados){	
		var htmlChildren = ""

		$.each(response, function (i,value){
			if(Area==value.CATEGORIA){
				
				htmlChildren = htmlChildren+"<tr class='datarow"+idt+"'>"+"<td class='notexport'></td>"+
				"<td>"+value.ID+"</td>"+
				"<td class='areaName wrapped' data-container='body' data-toggle='tooltip' title='" + value.ID + '-' + value.ASUNTO + "'>" + value.ASUNTO + "</td>"+
				
				"<td>"+ value.CONTRATO+"</td>" + 
				"<td>"+ value.PROYECTO+"</td>" +
				"<td>"+ value.ESTADO+"</td>" +
			    "<td>"+ parseDate(value.INICIO)+"</td>" +
				"<td>"+ parseDate(value.FIN)+"</td>" + 
				"<td>"+ value.VERSION+"</td>" + 
				"<td>"+ value.DESVIACION_T+"</td>" + 
				"<td>"+ toEuros(value.IMPORTE)+"</td>" + 
				"<td>"+ value.DESVIACION_I+"</td>" +
				"<td class='notexport'>" + alphabet[idt-1] + idt  + "</td></tr>"
											  
			}	
		});
		return htmlChildren;
	}	
	
	return {getDataArea:getDataArea}
}

function dataToHistogram(jsonResponse){
	
	var histog = ['Proyectos']
	categories=[]
	for (var i in jsonResponse){
			categories.push(jsonResponse[i]['PROYECTO']);
			histog.push(jsonResponse[i]['IMPORTE']);
			
		
	}
	console.log(histog);
	
	return histog;
}
	

function desgloseImportesEstado(jsonResponse) {
	
	var total = 0;
	var data = [];
	
	
	$(jsonResponse).each(function (i, d){
		data.push([d.ESTADO,d.IMPORTE]
		);
	});


	
	
	
	
	var ancho =$(this)[0].outerWidth*0.35;
	var alto = 300;

	c3.generate({
		bindto: '#piechartstate',
		size: {
        height: alto,
        width: ancho
    },
		data: {
			columns: data,
			type : 'pie'
		}
	});
	
	
	
	
	
	
/*	
    Highcharts.chart('piechartkey', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Tipo de importe (%)',
            widthAdjust: 0,
            align: 'center',
            verticalAlign: 'top'
        },

        credits: {
            enabled: false
        },

        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.2f} %</b>'
        },
        plotOptions: {
            pie: {
                size: '70%',
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    distance: -30,
                    format: '{point.percentage:.2f}',
                    style: {
                        color: 'white',
                        textShadow: '0 0 0px white',
                        fontWeight: 'normal'
                    }
                },
                showInLegend: true
            }
        },
		colors: ['#60A979','#E8E183','#808284'],
        series: serie
    });
	*/
}

function desgloseImportesCategoria(jsonResponse) {
	
	 var total = 0;
	 var data = [];
	 
	
	
	 $(jsonResponse).each(function (i, d){
		 data.push([d.CATEGORIA,d.IMPORTE]
		 );
	 });

	 var ancho =$(this)[0].outerWidth*0.40;
	 var alto = 300;

	 c3.generate({
		 bindto: '#piechartcategory',
		 size: {
         height: alto,
         width: ancho
     },
		 data: {
			 columns: data,
			 type : 'pie'
		 }
	 });
}

function mensualEvolution(){
	
	
	var ancho =$(this)[0].outerWidth*0.80;
	
	var alto = 400;

	var NL = d3.formatLocale ({
  "decimal": ",",
  "thousands": ".",
  "grouping": [3],
  "currency": ["", "€"],
  "dateTime": "%a %b %e %X %Y",
  "date": "%m/%d/%Y",
  "time": "%H:%M:%S",
  "periods": ["AM", "PM"],
  "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
  "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
  "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
  "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
});
	
	c3.generate({
		bindto: '#histogram',
		data: {
          columns: [histogramData],
          type: 'bar',
		  labels: {
            format: NL.format("$,.2f")
        
        }
		},
		color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
		},
		
        axis: {
		  x: {
            type: 'category',
            categories: categories
        }, 
          y: {
            label: {
              text: 'Euros',
              position: 'outer-middle',
			  format: NL.format("$,.2f")
            }
          }
        },
		tooltip: {
        format: {
            value: NL.format("$,.2f")
//            value: d3.format(',') // apply this format to both y and y2
        }
    },
		size: {height:alto, width:ancho},
        legend: {
          show: false
        }
      });
	
	
	
  /*Highcharts.chart('histogram', {

	  chart: {
		type: 'column'
	  },

	  title: {
		text: 'Importe por estado y mes'
	  },

	  xAxis: {
		categories: categories
	  },

	  yAxis: {
		allowDecimals: false,
		min: 0,
		title: {
		  text: 'Importe'
		}
	  },

	  tooltip: {
		formatter: function () {
		  return '<b>' + this.x + '</b><br/>' +
			this.series.name + ': ' + (this.y).toLocaleString() + ' euros.<br/>';
		}
	  },
	  credits: {
		enabled: false
	  },
	  series: histogramData,
	  colors: ['#60A979','#E8E183','#808284']
	});*/
}

/****************************************************************************************************************************************/

function removeLoading() {
	$("#loading").fadeOut("slow");
	$(".hiddenWhileLoading").fadeIn("slow");
};

//# sourceURL=economicoTEO.js 




