select g.`EJERCICIO`, g.`DESCRIPCION`, g.`IMPORTE`, d.`DESCRIPCION LARGA`, d1.`DENOMINACION LARGA`, d2.`DESCRIPCION LARGA`, d3.`DENOMINACION LARGA` from (select * from gastos g where g.`L_ESTADO` = 'M') g
inner JOIN dwh.dim_economica d ON  d.`GASTO/INGRESO` = 'G' and d.`SUBCONCEPTO` = g.`ECONOMICA`
inner JOIN dwh.dim_organica d1 on d1.`CENTRO GESTOR` = g.`CENTRO GESTOR`
inner join dwh.dim_funcional d2 on d2.`SUBFUNCION` = g.`FUNCIONAL`
LEFT JOIN dwh.dim_financiacion d3 ON d3.`GASTO/INGRESO` = 'G' AND d3.`FINANCIACION` = g.`FINANCIACION`